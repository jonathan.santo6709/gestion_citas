<?php
      class Usuarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");
            //validando si alguien esta conectado
            if ($this->session->userdata("c0nectadoUTC")) {
              // si esta conectado
              if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR")
              {
                // SI ES ADMINISTRADOR
              } else {
                redirect("/");
              }
            } else {
              redirect("seguridades/formularioLogin");
            }
        }

      public function index(){
        $this->load->view("header");
        $this->load->view("usuarios/index");
        $this->load->view("footer");
      }
      //funcion que rendizara la vista listado.php
      public function listado(){
        $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
        $this->load->view("usuarios/listado",$data);
      }

      public function nuevo(){
            $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
            $this->load->view('header');
            $this->load->view('usuarios/nuevo',$data);
            $this->load->view('footer');
          }
          public function editar($id_usu){
          //  $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
            $data["usuario"]=$this->usuario->consultarPorId($id_usu);
            $this->load->view('header');
            $this->load->view('usuarios/editar',$data);
            $this->load->view('footer');
          }
          public function procesarActualizacion(){
            $id_usu=$this->input->post("id_usu");
            $datosUsuarioEditado=array(
              "apellido_usu"=>$this->input->post("apellido_usu"),
              "nombre_usu"=>$this->input->post("nombre_usu"),
              "email_usu"=>$this->input->post("email_usu"),
              "password_usu"=>($this->input->post("password_usu")),
              "perfil_usu"=>$this->input->post("perfil_usu"),
              "estado_usu"=>$this->input->post("estado_usu"),
              "fecha_creacion_usu"=>$this->input->post("fecha_creacion_usu")
            );

            if ($this->usuario->actualizar($id_usu,$datosUsuarioEditado)) {
              //echo "INSERCION EXITOSA";
              $this->session->set_flashdata('confirmacion','USUARIO EDITADO EXITOSAMENTE');
            } else {
              $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
            }
            redirect("usuarios/index");
          }

          //registro de usuarios
          public function insertarUsuario(){
            $datosNuevoUsuario=array(
              "apellido_usu"=>$this->input->post("apellido_usu"),
              "nombre_usu"=>$this->input->post("nombre_usu"),
              "email_usu"=>$this->input->post("email_usu"),
              "password_usu"=>($this->input->post("password_usu")),
              "perfil_usu"=>$this->input->post("perfil_usu"),
              "estado_usu"=>$this->input->post("estado_usu"),
              "fecha_creacion_usu"=>$this->input->post("fecha_creacion_usu")
            );
            if ($this->usuario->insertarUsuario($datosNuevoUsuario)) {
                $this->session->set_flashdata("confirmacion","USUARIO INSERTADO EXITOSAMENTE.");
                } else {
                  $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
                }
                redirect("usuarios/index");

                }

                public function procesarEliminacion($id_usu){
                  if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
                $data['usuario']=$this->usuario->obtenerPorId($id_usu);
                    if($this->usuario->eliminar($id_usu)){
                        redirect("usuarios/index");
              }
                  else{
                        echo "ERROR AL ELIMINAR";
                      }
                    } else {
                      redirect("seguridades/formularioLogin");
              }
            }





}//cierre de la clase











      //

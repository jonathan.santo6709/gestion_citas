<?php

    //constructor
    class Cita extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('cita',$datos);
        }

        public function actualizar($id_ci,$datos){
          $this->db->where("id_ci",$id_ci);
            return $this->db->update("cita",$datos);
        }
        public function consultarPorId($id_ci){
          $this->db->where("id_ci",$id_ci);
            $cita=$this->db->get('cita');
            if ($cita->num_rows()>0) {
                // Cuando si hay citas registrados
                return $cita->row();
            } else {
                //cuando no hay cita
                return false;
            }

        }


        public function eliminar($id_ci){
        $this->db->where("id_ci",$id_ci);
        return $this->db->delete("cita");
    }
        public function obtenerPorId($id_ci){
           $this->db->where('id_ci',$id_ci);
           $query= $this->db->get('cita');
           if ($query->num_rows()>0) {
             // code...
             return $query->row();
           } else {
             return false;
           }
       }


      public function insertarCita($data){
        return $this->db->insert("cita",$data);
      }

      public function obtenerTodos(){
        $listado=$this->db->get("cita");
        if($listado->num_rows()>0){
          return $listado;
        }else{
          return false;
        }
      }

    }//cierre de la clase

 ?>

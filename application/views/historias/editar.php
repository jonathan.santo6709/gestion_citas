<div class="col-md-12">
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
          <br>
            <center>
              <br>
              <h2> <b>Editar Historia Clinica</b> </h2>
              <hr>
              <br>
            </center>
            <form action="<?php echo site_url(); ?>/historias/procesarActualizacion" method="post" id="frm_actualizar_historia" >
              <input type="hidden" name="id_his" id="id_his" value="<?php echo $historia->id_his; ?>" >
              <label for="">Paciente</label>
              <select class="form-control" name="fk_id_pac" id="fk_id_pac">
                  <option value="">--Seleccione un paciente--</option>
                  <?php if ($listadoPacientes): ?>
                        <?php foreach ($listadoPacientes->result() as $pacienteTemporal): ?>
                              <option value="<?php echo $pacienteTemporal->id_pac ?>">
                                  <?php echo $pacienteTemporal->nombre_pac ?>
                                  <?php echo $pacienteTemporal->apellido_pac ?>
                              </option>
                        <?php endforeach; ?>
                  <?php endif; ?>
              </select>
              <br>
              <label for="">NUMERO HISTORIA:</label><br>
              <input type="number" name="numero_his" id="numero_his" value="<?php echo $historia->numero_his; ?>" placeholder="Ingrese el numero de historia Clinica" class="form-control" enable><br>
              <label for="">MOTIVO CONSULTA :</label><br>
              <input type="text" name="motivo_consulta_his" id="motivo_consulta_his" value="<?php echo $historia->motivo_consulta_his; ?>" placeholder="Ingrese el motivo de la consulta" class="form-control"><br>
              <label for="">ENFERMEDAD ACTUAL:</label><br>
              <input type="text" name="enfermedad_actual_his" id="enfermedad_actual_his" value="<?php echo $historia->enfermedad_actual_his; ?>" placeholder="Ingrese la enfermedad actual" class="form-control"><br>
              <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
              &nbsp;&nbsp;&nbsp
              <a href="<?php echo site_url(); ?>/historias/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
            <br>
            </form>
      </div>
      <div class="col-md-2"></div>
    </div>

</div>

<script type="text/javascript">
  $("#fk_id_pac").val("<?php echo $paciente->fk_id_pac; ?>");
</script>

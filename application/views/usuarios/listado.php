<center>
    <a href="<?php echo site_url(); ?>/usuarios/nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> AGREGAR</a>
    <button type="button" name="button" class="btn btn-primary" onclick="cargarListadoUsuarios();">ACTUALIZAR</button>
    <br>
    <br>
  </center>
<?php if ($listadoUsuarios): ?>
  <div class="table-responsive">
  <table class="table table-bordered table-hover" id="tbl-usuarios">
      <thead>
         <tr>
           <th class="text-center">ID</th>
           <th class="text-center">APELLIDO</th>
           <th class="text-center">NOMBRE</th>
           <th class="text-center">EMAIL</th>
           <th class="text-center">PERFIL</th>
           <th class="text-center">ESTADO</th>
           <th class="text-center">FECHA CREACION</th>
           <th class="text-center">ACCIONES</th>
         </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoUsuarios->result() as $usuario): ?>
              <tr>
                  <td class="text-center">
                    <?php echo $usuario->id_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->apellido_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->nombre_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->email_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->perfil_usu; ?>
                  </td>
                  <td class="text-center">
                  <?php if ($usuario->estado_usu=="1"): ?>
                    <div class="alert alert-success">
                      ACTIVO
                      <!-- <?php echo $usuario->estado_usu;?> -->
                    </div>
                  <?php else: ?>
                    <div class="alert alert-danger">
                      INACTIVO
                      <!-- <?php echo $usuario->estado_usu;?> -->
                    </div>

                  <?php endif; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->fecha_creacion_usu; ?>
                  </td>
                  <td class="text-center">
                    <a href="<?php echo site_url(); ?>/usuarios/editar/<?php echo $usuario->id_usu;?>"class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a onclick="confirmarEliminacion('<?php echo $usuario->id_usu; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                  </td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
  </div>
<?php else: ?>
    <br>
    <div class="alert alert-danger">
        No se encontraron Usuarios Registrados
    </div>
<?php endif; ?>
<script type="text/javascript">
    function confirmarEliminacion(id_usu){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el usuario de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/usuarios/procesarEliminacion/"+id_usu;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
  $(document).ready( function () {
  	$('#tbl-usuarios').DataTable({
      dom: 'Blfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
    });
  } );
</script>

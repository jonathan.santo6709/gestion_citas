<?php
  class Historias extends CI_Controller{
      public function __construct(){
          parent::__construct();
          $this->load->model("historia");
          $this->load->model("paciente");
          //validacion
      }
      public function index(){
          $data["listadoHistorias"]=$this->historia->consultarTodos();
          $this->load->view('header');
          $this->load->view('historias/index',$data);
          $this->load->view('footer');
      }
      //funcion Nuevo
      public function nuevo(){
        //modelo
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $this->load->view('header');
          $this->load->view('historias/nuevo',$data);
          $this->load->view('footer');
      }
      //funcion editar
      public function editar($id_his){
        //modelo
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $data["historia"]=$this->historia->consultarPorId($id_his);
          $this->load->view('header');
          $this->load->view('historias/editar',$data);
          $this->load->view('footer');
      }
      public function procesarActualizacion(){
        $id_his=$this->input->post("id_his");
        $datosHistoriaEditado=array(
            "numero_his"=>$this->input->post("numero_his"),
            "motivo_consulta_his"=>$this->input->post("motivo_consulta_his"),
            "enfermedad_actual_his"=>$this->input->post("enfermedad_actual_his"),
            "fk_id_pac"=>$this->input->post("fk_id_pac"),
          );
          if ($this->historia->actualizar($id_his,$datosHistoriaEditado)){
            $this->session->set_flashdata("confirmacion","Historia Actualizada exitosamente.");
          } else {
              $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
          }
          redirect("historias/index");
      }

      public function guardarHistoria(){
        $datosNuevoHistoria=array(
          "numero_his"=>$this->input->post("numero_his"),
          "motivo_consulta_his"=>$this->input->post("motivo_consulta_his"),
          "enfermedad_actual_his"=>$this->input->post("enfermedad_actual_his"),
          "fk_id_pac"=>$this->input->post("fk_id_pac"),
        );

        if ($this->historia->insertar($datosNuevoHistoria)) {
          //nombre variable contenido variable
              $this->session->set_flashdata("confirmacion","Historia clinica insertada exitosamente.");
        } else {
              $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
        }
        redirect("historias/index");
      }

      function procesarEliminacion($id_his){
              if ($this->historia->eliminar($id_his)) {
                $this->session->set_flashdata("confirmacion","Historia clinica eliminado exitosamente.");
              } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("historias/index");
      }
    }//cierre funcion
 ?>

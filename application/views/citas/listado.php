<center>
    <a href="<?php echo site_url(); ?>/citas/nuevo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> AGREGAR</a>
    <button type="button" name="button" class="btn btn-primary" onclick="cargarListadoCitas();">ACTUALIZAR</button>
    <br>
    <br>
  </center>
<br>
<?php if ($listadoCitas): ?>
  <table class="table table-bordered table-striped table-hover" id="tbl-citas">
      <thead>
         <tr>
           <th class="text-center">ID</th>
           <th class="text-center">IDENTIFICACION</th>
           <th class="text-center">NOMBREs</th>
           <th class="text-center">APELLIDOS</th>
           <th class="text-center">MOTIVO DE LA CONSULTA</th>
           <th class="text-center">TIPO</th>
           <th class="text-center">ACCIONES</th>
         </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoCitas->result() as $filaTemporal): ?>
              <tr>
                  <td class="text-center">
                    <?php echo $filaTemporal->id_ci; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->identificacion_ci; ?>
                  </td>
                  <td class="text-center">
                      <?php echo $filaTemporal->nombre_ci; ?>
                  </td>
                  <td class="text-center">
                      <?php echo $filaTemporal->apellido_ci; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->detalle_ci; ?>
                  </td>

                  <td class="text-center">
                  <?php if ($filaTemporal->tipo_ci=="AFILIADO"): ?>
                    <div class="alert alert-success">
                      <?php echo $filaTemporal->tipo_ci;?>
                    </div>
                  <?php else: ?>
                    <div class="alert alert-danger">
                      <?php echo $filaTemporal->tipo_ci;?>
                    </div>

                  <?php endif; ?>
                  </td>

                  <td class="text-center">
                    <a href="<?php echo site_url(); ?>/citas/editar/<?php echo $filaTemporal->id_ci;?>"class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_ci; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                  </td>

              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
<?php else: ?>
    <br>
    <div class="alert alert-danger">
        No se encontraron citas Registrados
    </div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_ci){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cita de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/citas/procesarEliminacion/"+id_ci;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<br>
<br>
<script type="text/javascript">
  $(document).ready( function () {
  	$('#tbl-citas').DataTable({
      dom: 'Blfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
    });
  } );
</script>

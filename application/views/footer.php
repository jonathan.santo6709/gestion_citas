
<br>
<br>

<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">
      </script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<!-- gggggggggggggggggggggggggggggg -->
</div>
</div>

       </div>
       <!-- content-wrapper ends -->
       <!-- partial:partials/_footer.html -->
       <footer class="footer">
           <div class="card">
               <div class="card-body">
                   <div class="d-sm-flex justify-content-center justify-content-sm-between">
                       <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                       <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center text-muted">Free <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">Bootstrap dashboard</a> templates from Bootstrapdash.com</span>
                   </div>
               </div>
           </div>
       </footer>
       <!-- partial -->
     </div>
     <!-- main-panel ends -->
   </div>
   <!-- page-body-wrapper ends -->
 </div>
 <!-- container-scroller -->

 <!-- base:js -->
 <script src="<?php echo base_url();?>/assests/vendors/js/vendor.bundle.base.js"></script>
 <!-- endinject -->
 <!-- Plugin js for this page-->
 <script src="<?php echo base_url();?>/assests/vendors/chart.js/Chart.min.js"></script>
 <!-- End plugin js for this page-->
 <!-- inject:js -->
 <script src="<?php echo base_url();?>/assests/js/off-canvas.js"></script>
 <script src="<?php echo base_url();?>/assests/js/hoverable-collapse.js"></script>
 <script src="<?php echo base_url();?>/assests/js/template.js"></script>
 <script src="<?php echo base_url();?>/assests/js/settings.js"></script>
 <script src="<?php echo base_url();?>/assests/js/todolist.js"></script>
 <!-- endinject -->
 <!-- Custom js for this page-->
 <script src="<?php echo base_url();?>/assests/js/dashboard.js"></script>
 <?php if ($this->session->flashdata('confirmacion')): ?>
   <script type="text/javascript">
     iziToast.success({
       title: 'CONFIRMACION',
       message: '<?php echo $this->session->flashdata('confirmacion'); ?>',
       position: 'topRight',
     });
   </script>
<?php endif; ?>

<?php if ($this->session->flashdata('error')): ?>
  <script type="text/javascript">
    iziToast.danger({
      title: 'ADVERTENCIA',
      message: '<?php echo $this->session->flashdata('error'); ?>',
      position: 'topRight',
    });
  </script>
 <?php endif; ?>


 <?php if ($this->session->flashdata("bienvenida")): ?>
   <script type="text/javascript">
     iziToast.info({
          title: 'CONFIRMACIÓN',
          message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
          position: 'topRight',
        });
   </script>
 <?php endif; ?>
 <!-- End custom js for this page-->
 <style media="screen">
     .error{
       color:red;
       font-size: 16px;
     }
     input.error, select.error{
       border: 2px solid red;
     }
 </style>


</body>

</html>

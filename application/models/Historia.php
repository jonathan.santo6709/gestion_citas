<?php
    //constructor
    class Historia extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }
        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('historia',$datos);

        }

        public function actualizar($id_his,$datos){
          $this->db->where("id_his",$id_his);
            return $this->db->update("historia",$datos);
        }

        public function consultarPorId($id_his){
          $this->db->where("id_his",$id_his);
          $this->db->join("paciente","paciente.id_pac=historia.fk_id_pac");
            $historia=$this->db->get('historia');
            if ($historia->num_rows()>0) {
                // Cuando si hay registrados
                return $historia->row();
            } else {
                //cuando no hay registros
                return false;
            }

        }

        //funcion para consultar
        public function consultarTodos(){
          $this->db->join("paciente","paciente.id_pac=historia.fk_id_pac");
            $listadoHistorias=$this->db->get('historia');
            if ($listadoHistorias->num_rows()>0) {
                // Cuando si hay registrados
                return $listadoHistorias;
            } else {
                //cuando no hay registros
                return false;
            }
        }

        public function eliminar($id_his){
          $this->db->where("id_his",$id_his);
          return $this->db->delete("historia");
        }
    }
 ?>

<div class="col-md-12">
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
          <br>
            <center>
              <br>
              <h2> <b>Nueva Historia Clinica</b> </h2>
              <hr>
              <br>
            </center>
            <form action="<?php echo site_url(); ?>/historias/guardarHistoria" method="post" id="frm_nuevo_historia" enctype="multipart/form-data" accept="image/*">
              <label for="">Paciente</label>
              <select class="form-control" name="fk_id_pac" id="fk_id_pac">
                  <option value="">--Seleccione un paciente--</option>
                  <?php if ($listadoPacientes): ?>
                        <?php foreach ($listadoPacientes->result() as $pacienteTemporal): ?>
                              <option value="<?php echo $pacienteTemporal->id_pac ?>">
                                  <?php echo $pacienteTemporal->nombre_pac ?>
                                  <?php echo $pacienteTemporal->apellido_pac ?>
                              </option>
                        <?php endforeach; ?>
                  <?php endif; ?>
              </select>
              <br>
              <label for="">NUMERO HISTORIA:</label><br>
              <input type="number" name="numero_his" id="numero_his" value="" placeholder="Ingrese el numero de historia Clinica" class="form-control" enable><br>
              <label for="">MOTIVO CONSULTA :</label><br>
              <input type="text" name="motivo_consulta_his" id="motivo_consulta_his" value="" placeholder="Ingrese el motivo de la consulta" class="form-control"><br>
              <label for="">ENFERMEDAD ACTUAL:</label><br>
              <input type="text" name="enfermedad_actual_his" id="enfermedad_actual_his" value="" placeholder="Ingrese la enfermedad actual" class="form-control"><br>
              <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Guardar</button>
              &nbsp;&nbsp;&nbsp
              <a href="<?php echo site_url(); ?>/historias/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
              <br>
              </form>
      </div>
      <div class="col-md-2"></div>
    </div>

</div>

<script type="text/javascript">
    $("#frm_nuevo_historia").validate({
      rules:{
        fk_id_pac:{
          required:true
        },
        numero_his:{
          required:true,
        },
        motivo_consulta_his:{
          required:true
        }
        enfermedad_actual_his:{
          required:true
        }
      },


      messages:{
        fk_id_pac:{
          required:"Por favor seleccione uno "
        },
        numero_his:{
          required:"por favor ingrese un numero"
        },
        motivo_consulta_his:{
          required:"por favor ingrese un motivo"
        }
        enfermedad_actual_his:{
          required:"por favor ingrese el numero de telefono"
        }
      }
    });
</script>

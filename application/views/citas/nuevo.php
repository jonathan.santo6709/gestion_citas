<br>
<form  action="<?php echo site_url(); ?>/citas/guardarCita" method="post" id="frm_nuevo_cita"  enctype="multipart/form-data">
<center>
  <h1> <b>NUEVA CITA</b> </h1>
</center>
<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-md-12">
    <br>
      <label for"">IDENTIFICACION:</label>
      <br>
      <input type="number" class="form-control" value="" type="text" name="identificacion_ci" id= "identificacion_ci" placeholder="Ingrese el numero de identificacion">
      <br>
      <label for"">NOMBRE:</label>
      <br>
      <input type="text" class="form-control" value="" type="text" name="nombre_ci" id= "nombre_ci" placeholder="Ingrese el nombre">
      <br>
      <label for"">APELLIDO:</label>
        <br>
        <input type="text" class="form-control" value="" type="text" name="apellido_ci" id= "apellido_ci" placeholder="Ingrese el apellido">
        <br>
        <label for"">DETALLE:</label>
          <br>
          <input type="text" class="form-control" value="" type="text" name="detalle_ci" id= "detalle_ci" placeholder="Ingrese el detalle"><br>
          <div class="row">
            <div class="col-md-6">
              <label for"">FECHA:</label>
              <br>
              <input type="date" class="form-control" value="" type="text" name="fecha_ci" id= "fecha_ci" placeholder="Ingrese el detalle"><br>
            </div>
            <div class="col-md-6">
              <label for"">HORA:</label>
              <br>
              <input type="time" class="form-control" value="" type="text" name="hora_ci" id= "hora_ci" placeholder="Ingrese el detalle"><br>
            </div>
          </div>

      <label for"">TIPO:</label>
      <select class="form-control" type="text" style="width:100% "name="tipo_ci" id="tipo_ci">
      <option value="">--Seleccione--</option>
      <option value="AFILIADO">AFILIADO</option>
      <option value="NO AFILIADO">NO AFILIADO</option>
    </select>
    <br>
    <div class="row">
    <div class="col-md-12 mt-4 mb-4 text-center">
      <button type="submit" class="btn btn-success" style="width:20%"><b><i class="fa fa-save"> </i>&nbsp;&nbsp;&nbsp;Guardar</b></button>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php site_url(''); ?>../citas/index">
      <button type="button" class="btn btn-danger" style="width:20%"><b><i class="fa fa-times"> </i>&nbsp;&nbsp;&nbsp;Cancelar</b></button>
      </a>
    </div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_cita').validate({
      rules:{
        identificacion_ci:{
          letras:false,
          required:true,
        },
        nombre_ci:{
          letras:true,
          required:true,
        },
        apellido_ci:{
          letras:true,
          required:true,
        },
        detalle_ci:{
          letras:true,
          required:true,
        },
      tipo_ci:{
          required:true,
        }



      },
      messages:{
        identificacion_ci:{
          letras:"cedula incorrecta",
          required:"Por favor ingrese la cedula correcta"

        },

        nombre_ci:{
          letras:"Nombre incorrecto",
          required:"Por favor ingrese el Nombre"

        },
        apellido_ci:{
          letras:"Apellido incorrecto",
          required:"Por favor ingrese el Apellido"

        },
        detalle_ci:{
          letras:"descripcion incorrecto",
          required:"Por favor ingrese la descripcion"

        },

        tipo_ci:{
          required:"Seleccione el tipo"
        }
      }
  });
</script>
</script>

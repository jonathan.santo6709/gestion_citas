<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <br>
          <h2 class="card-title"> NUEVO PACIENTE </h2><hr>
        </div>
        </div>
        <form action="<?php echo site_url() ?>/pacientes/guardarPaciente" method="post" id="frm_nuevo_paciente">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <label for="" class="card-title">Identificacion:</label><br>
                <input type="number" name="identificacion_pac" id="identificacion_pac" value="" placeholder="Ingrese el Numero de Cedula" class="form-control"><br>
                <label for="" class="card-title">Apellidos:</label><br>
                <input type="text" name="apellido_pac" id="apellido_pac" value="" placeholder="Ingrese los Apellidos" class="form-control"><br>
                <label for="" class="card-title">Nombres:</label><br>
                <input type="text" name="nombre_pac" id="nombre_pac" value="" placeholder="Ingrese los Nombres" class="form-control"><br>
                <label for="" class="card-title">Sexo:</label><br>
                <select class="form-control" name="sexo_pac" id="sexo_pac">
                    <option value="">--- Seleccione uno ---</option>
                    <option value="MASCULINO">MASCULINO</option>
                    <option value="FEMENINO">FEMENINO</option>
                </select><br>
                <label for="" class="card-title">Edad:</label><br>
                <input type="number" name="edad_pac" id="edad_pac" value="" placeholder="Ingrese la edad" class="form-control"><br>
              </div>
              <div class="col-md-6">
                <label for="" class="card-title">Estado Civil:</label><br>
                <select class="form-control" name="estado_civil_pac" id="estado_civil_pac">
                    <option value="">--- Seleccione uno ---</option>
                    <option value="SOLTERO">SOLTERO</option>
                    <option value="UNION LIBRE">UNION LIBRE</option>
                    <option value="CASADO">CASADO</option>
                    <option value="VIUDO">VIUDO</option>
                </select><br>
                <label for="" class="card-title">Correo Electronico:</label><br>
                <input type="text" name="email_pac" id="email_pac" value="" placeholder="Ingrese el Correo Electronico" class="form-control"><br>
                <label for="" class="card-title">Numero Telefonico:</label><br>
                <input type="number" name="telefono_pac" id="telefono_pac" value="" placeholder="Ingrese el numero de telefono" class="form-control"><br>
                <label for="" class="card-title">Direccion:</label><br>
                <input type="text" name="direccion_pac" id="direccion_pac" value="" placeholder="Ingrese la direccion" class="form-control"><br>
                <label for="" class="card-title">Estado:</label><br>
                <select class="form-control" name="estado_pac" id="estado_pac">
                    <option value="">--- Seleccione un estado ---</option>
                    <option value="ACTIVO">ACTIVO</option>
                    <option value="INACTIVO">INACTIVO</option>
                </select><br>
              </div>
            </div>
              <div class="text-center">
                <button type="submit" class="btn btn-success" name="button"> <i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                &nbsp;&nbsp;&nbsp
                <a href="<?php echo site_url(); ?>/pacientes/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
                <br><br>
              </div>
            </div>
          </div>
        <br>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#frm_nuevo_paciente').validate({
      rules:{
        identificacion_pac:{
          letras:false,
          required:true,
        },
        nombre_pac:{
          letras:true,
          required:true,
        },
        apellido_pac:{
          letras:true,
          required:true,
        },
      sexo_pac:{
          required:true,
        },
      sexo_pac:{
            required:true,
          }



      },
      messages:{
        identificacion_ci:{
          letras:"cedula incorrecta",
          required:"Por favor ingrese la cedula correcta"

        },

        nombre_ci:{
          letras:"Nombre incorrecto",
          required:"Por favor ingrese el Nombre"

        },
        apellido_ci:{
          letras:"Apellido incorrecto",
          required:"Por favor ingrese el Apellido"

        },
        detalle_ci:{
          letras:"descripcion incorrecto",
          required:"Por favo ingrese la descripcion"

        },

        tipo_ci:{
          required:true
        }
      }
  });
</script>

<?php
    class Citas extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('cita');
          $this->load->model('paciente');
          //validando si alguien esta conectado
          if ($this->session->userdata("c0nectadoUTC")) {
            // si esta conectado
          } else {
            redirect("seguridades/formularioLogin");
          }
          }
          public function index(){
            $data["listadoCitas"]=$this->cita->obtenerTodos();
            $this->load->view('header');
            $this->load->view('citas/index',$data);
            $this->load->view('footer');
          }
          //funcion que renderiza la vista listado.php
          public function listado(){
            $data["listadoCitas"]=$this->cita->obtenerTodos();
            $this->load->view('citas/listado',$data);
          }
          public function nuevo(){
            // $data["listadoCitas"]=$this->cita->obtenerTodos();
            $data["listadoPacientes"]=$this->paciente->consultarTodos();
            $this->load->view('header');
            $this->load->view('citas/nuevo',$data);
            $this->load->view('footer');
          }
          public function editar($id_ci){
            $data["listadoPacientes"]=$this->paciente->consultarTodos();
            // $data["listadoCitas"]=$this->cita->obtenerTodos();
            $data["cita"]=$this->cita->consultarPorId($id_ci);
            $this->load->view('header');
            $this->load->view('citas/editar',$data);
            $this->load->view('footer');
          }
          public function procesarActualizacion(){
            $id_ci=$this->input->post("id_ci");
            $datosCitaEditado=array(
              "identificacion_ci"=>$this->input->post("identificacion_ci"),
              "nombre_ci"=>$this->input->post("nombre_ci"),
              "apellido_ci"=>$this->input->post("apellido_ci"),
              "detalle_ci"=>$this->input->post("detalle_ci"),
              "tipo_ci"=>$this->input->post("tipo_ci"),
              "fecha_ci"=>$this->input->post("fecha_ci"),
              "hora_ci"=>$this->input->post("hora_ci"),
              "fk_id_pac"=>$this->input->post("fk_id_pac")
            );

            if ($this->cita->actualizar($id_ci,$datosCitaEditado)) {
              //echo "INSERCION EXITOSA";
              $this->session->set_flashdata('confirmacion','CITA EDITADO EXITOSAMENTE');
            } else {
              $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
            }
            redirect("citas/index");
          }

          //registro de usuarios
          public function guardarCita(){
            $datosNuevoCita=array(
              "identificacion_ci"=>$this->input->post("identificacion_ci"),
              "nombre_ci"=>$this->input->post("nombre_ci"),
              "apellido_ci"=>$this->input->post("apellido_ci"),
              "detalle_ci"=>$this->input->post("detalle_ci"),
              "tipo_ci"=>$this->input->post("tipo_ci"),
              "fecha_ci"=>$this->input->post("fecha_ci"),
              "hora_ci"=>$this->input->post("hora_ci"),
              "fk_id_pac"=>$this->input->post("fk_id_pac")
            );

            if ($this->cita->insertar($datosNuevoCita)) {
                $this->session->set_flashdata("confirmacion","CITA INSERTADO EXITOSAMENTE.");
                } else {
                  $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
                }
                redirect("citas/index");

                }

                public function procesarEliminacion($id_ci){
                  if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
                $data['cita']=$this->cita->obtenerPorId($id_ci);
                    if($this->cita->eliminar($id_ci)){
                        redirect("citas/index");
              }
                  else{
                        echo "ERROR AL ELIMINAR";
                      }
                    } else {
                      redirect("seguridades/formularioLogin");
              }
            }
}//cierre de clase
?>

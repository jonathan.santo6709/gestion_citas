<br>
<h1 class="text-center">Gestion de Cita</h1>
<hr>
<div class="row">
  <div class="col-md-12" id="contenedor-listado-citas">
    <i class="fa fa-spin  fa-lg fa-spinner"></i>
    CONSULTANDO DATOS
  </div>

</div>

<script type="text/javascript">
  function cargarListadoCitas(){
    $("#contenedor-listado-citas").html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
    $("#contenedor-listado-citas").load("<?php echo site_url(); ?>/citas/listado");
  }
  cargarListadoCitas();
</script>


<h1 class="text-center">Gestion de Usuarios</h1>
<hr>
<div class="row">
  <div class="col-md-12" id="contenedor-listado-usuarios">
    <i class="fa fa-spin  fa-lg fa-spinner"></i>
    CONSULTANDO DATOS
  </div>

</div>
<button type="button" name="button" class="btn btn-primary"
 onclick="cargarListadoUsuarios();">Actualizar</button>


 <!-- Trigger the modal with a button -->


 <!-- Modal -->
 <div id="modalNuevoUsuario"
 style="z-index:9999 !important;"
  class="modal fade" role="dialog">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title"><i class="fa fa-users"></i> Nuevo Usuario</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
         <form class=""
         action="<?php echo site_url('usuarios/insertarUsuario'); ?>"
         method="post"
         id="frm_nuevo_usuario">
         <label for="">APELLIDO:</label>
         <br>
         <input type="text" name="apellido_usu"
         id="apellido_usu" class="form-control"> <br>

         <label for="">NOMBRE:</label>
         <br>
         <input type="text" name="nombre_usu"
         id="nombre_usu" class="form-control"> <br>

         <label for="">EMAIL:</label>
         <br>
         <input type="text" name="email_usu"
         id="email_usu" class="form-control"> <br>

         <label for="">CONTRASEÑA:</label>
         <br>
         <input type="password" name="password_usu"
         id="password_usu" class="form-control"> <br>

         <label for="">CONFIRME LA CONTRASEÑA:</label>
         <br>
         <input type="password" name="password_confirmada"
         id="password_confirmada" class="form-control"> <br>

         <label for="">PERFIL:</label>
         <br>
         <select class="form-control" name="perfil_usu"
         id="perfil_usu">
          <option value="">Seleccione una opción</option>
          <option value="ADMINISTRADOR">ADMINISTRADOR</option>
          <option value="MEDICO">MEDICO</option>
         </select>
         <br>
         <button type="submit" name="button"
         class="btn btn-success">
           <i class="fa fa-save"></i> Guardar
         </button>

         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>
<script type="text/javascript">
  function cargarListadoUsuarios(){
    $("#contenedor-listado-usuarios").html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
    $("#contenedor-listado-usuarios").load("<?php echo site_url(); ?>/usuarios/listado");
  }
  cargarListadoUsuarios();
</script>

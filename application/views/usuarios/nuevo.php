<br>
<form  action="<?php echo site_url(); ?>/usuarios/insertarUsuario" method="post" id="frm_nuevo_usuario"  enctype="multipart/form-data">

<br>
<br>
<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-md-12">

    <label for"">Nombre:</label>
    <br>
    <input type="text" class="form-control" value=""  name="nombre_usu" id= "nombre_usu"value="" placeholder="Ingrese su nombre">
    <br>
    <label for"">Apellido:</label>
    <br>
    <input type="text" class="form-control" value=""  name="apellido_usu" id= "apellido_usu"value="" placeholder="Ingrese su Apellido">
    <br>
    <label for"">Email:</label>
    <br>
    <input type="text" class="form-control" value=""  name="email_usu" id= "email_usu"value="" placeholder="Ingrese su email">
    <br>
    <label for"">Contraseña:</label>
    <br>
    <input type="password" class="form-control" value=""  name="password_usu" id= "password_usu"value="" placeholder="Ingrese su contraseña">
    <br>
    <label for="">PERFIL:</label>
    <br>
    <select class="form-control" name="perfil_usu" id="perfil_usu">
     <option value="">Seleccione una opción</option>
     <option value="ADMINISTRADOR">ADMINISTRADOR</option>
     <option value="MEDICO">MEDICO</option>
     <option value="SECRETARIA">SECRETARIA</option>
    </select>
    <br>
    <label for="">ESTADO:</label>
    <br>
    <select class="form-control" name="estado_usu" id="estado_usu">
     <option value="">Seleccione una opción</option>
     <option value="1">1</option>
     <option value="0">0</option>
    </select>
    <br>
    <label for"">Fecha Creacion:</label>
    <br>
    <input type="date" class="form-control" value="" step="1" min="01-01-2022" max="31-12-2022"  name="fecha_creacion_usu" id="fecha_creacion_usu" >
    <br>

    <!-- <input type="date" class="form-control"  min="01/01/2022" max="31/12/2022"  name="fecha_creacion_usu" id="fecha_creacion_usu" ><br> -->
    <br>
    </div>
    </table>
    <div class="row">
    <div class="col-md-12 mt-4 mb-4 text-center">
      <button type="submit" class="btn btn-primary" style="width:20%"><b><i class="fa fa-times"></i>Guardar</b></button>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php site_url(''); ?>../usuarios/index">
      <button type="button" class="btn btn-primary" style="width:20%"><b><i class="fa fa-times"></i>Cancelar</b></button>
      </a>
    </div>
  </div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_usuario').validate({
      rules:{
        nombre_usu:{
          required:true,
          letras:true,
        },
        apellido_usu:{
          required:true,
          letras:true,
        },
        email_usu:{
          required:true,
          email:true,
        },
        password_usu:{
          required:true,
        },
        estado_usu:{
          required:true,
        },
        perfil_usu:{
          required:true,
        },
        fecha_creacion_usu:{
          required:true,
        },
      },
      messages:{
        nombre_usu:{
          letras:"Nombre incorrecto",
          required:"Por favo ingrese el Nombre"

        },
        apellido_usu:{
          letras:"Apellido incorrecto",
          required:"Por favo ingrese el Apellido"

        },
        email_usu:{
          required:"Solo acepta correos electronicos validos",
          email:"Correro incorrecto"
        },
        password_usu:{
          required:"Ingrese contraseña",
        },
        perfil_usu:{
          required:"Ingrese estado",
        },
        estado_usu:{
          required:"Ingrese Perfil",
        },
        fecha_creacion_usu:{
          required:"Ingrese fecha",
        }
      }



  });
</script>

<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/login/estilo_login.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<section class="vh-100" style="background-color: #E5E8E8 ;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">
            <form class="" action="<?php echo site_url();?>/seguridades/validarAcceso" method= "post">
            <h3 class="mb-4">Iniciar Sesion</h3>
            <hr>
            <!-- Email input -->
            <div class="form-outline mb-4">
              <input type="email" name="email_usu" id="email_usu" class="form-control form-control-lg"
                placeholder="Usuario o Email" required/>
              <label class="form-label" for="form3Example3"></label>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-3">
              <input type="password" name="password_usu" id="password_usu" class="form-control form-control-lg"
                placeholder="Contraseña" required/>
              <label class="form-label" for="form3Example4"></label>
            </div>

            <div>
              <center>
              <a href="#!">¿Olvidaste tu contraseña?</a>
              </center>
            </div>
            <br>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Iniciar Sesion</button>
            <p class="small fw-bold mt-2 pt-1 mb-0">¿No tienes una cuenta? <a href="#!"
                class="link-danger">Registrar</a></p>
              </form>
              <?php if ($this->session->flashdata("error")): ?>
              <script type="text/javascript">
                  alert("<?php echo $this->session->flashdata("error"); ?> ");
              </script>
              <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
